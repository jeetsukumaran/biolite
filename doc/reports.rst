Generating reports
==================

:mod:`report` Module
--------------------

.. automodule:: biolite.report
    :members:
    :undoc-members:
    :show-inheritance:

