.. BioLite documentation master file, created by
   sphinx-quickstart on Thu Apr  5 11:03:37 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BioLite Reference Manual
========================

BioLite is a Python/C++ framework for implementing bioinformatics pipelines for
Next-Generation Sequencing (NGS) data, in particular pair-end Illumina data.

BioLite is designed around three priorities:

* automating the collection and reporting of *diagnostics*;
* tracking *provenance* of analyses;
* and providing lightweight tools for building out customized analysis *pipelines*.

Where possible, we have wrapped existing bioinformatics tools, especially for
assembly, alignment and annotation.  For analyses where a tool does not exist
or is not optimized for the high computational and storage requirements of NGS
data, we have developed custom tools in C++ after the standard UNIX "`pipe and
filter`_" design pattern.

.. _pipe and filter: http://en.wikipedia.org/wiki/Unix_philosophy

Our primary motivation for developing BioLite is to implement Agalma_, a *de
novo* transcriptome assembly and phylogenomic analysis pipeline for Illumina
data.

.. _Agalma: http://www.dunnlab.org/agalma

Contents
========

.. toctree::
   :maxdepth: 4

   install
   catalog
   diagnostics
   pipelines
   reports
   wrappers
   workflows
   internals

Citing
======

BioLite is still under development, and is an experimental tool that should be
used with care.  Please cite:

Howison M, Sinnott-Armstrong NA, Dunn CW. 2012. `BioLite, a lightweight bioinformatics framework with automated tracking of diagnostics and provenance <https://www.usenix.org/conference/tapp12/biolite-lightweight-bioinformatics-framework-automated-tracking-diagnostics-and>`_.
In *Proceedings of the 4th USENIX Workshop on the Theory and Practice of
Provenance (TaPP '12)*, 14-15 June 2012, Boston, MA, USA.

BioLite makes use of many other programs that do much of the heavy lifting of
the analyses. Please be sure to credit these essential components as well.
Check the biolite.cfg file for web links to these programs, where you can find
more information on how to cite them.

Funding
=======

This software has been developed with support from the following US National
Science Foundation grants:

PSCIC Full Proposal: The iPlant Collaborative: A Cyberinfrastructure-Centered
Community for a New Plant Biology (Award Number 0735191)

Collaborative Research: Resolving old questions in Mollusc phylogenetics with
new EST data and developing general phylogenomic tools (Award Number 0844596)

Infrastructure to Advance Life Sciences in the Ocean State (Award Number
1004057)

The Brown University `Center for Computation and Visualization`_ has been
instrumental to the development of BioLite.

.. _Center for Computation and Visualization: http://www.ccv.brown.edu

License
=======

Copyright (c) 2012-2014 Brown University. All rights reserved.

BioLite is distributed under the GNU General Public License version
3. For more information, see LICENSE or visit:
`<http://www.gnu.org/licenses/gpl.html>`_

BioLite includes source code from the following projects:

* gzstream C++ interface v1.5, which is distributed under the GNU Lesser
  General Public License in `LICENSE.gzstream`
* Bootstrap v2.3.1 CSS style, which is distributed under the Apache License
  v2.0 in `share/bootstrap.min.css`
* jsphylosvg v1.55, which is distributed under the GPL in `LICENSE.jsphylosvg`,
  and which includes Raphael 1.4.3, which is distributed under the MIT license
* D3js v3.1.9, which is distributed under the BSD license in `LICENSE.d3js`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

